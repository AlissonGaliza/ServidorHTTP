
package logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author lumo
 */
public class MyLogger {
    
    private String path;
    private String name;
    private String IpCliente;

    /**
     *
     * @param path Caminho para salvar o log
     * @param name Nome da classe que utilizara o sistema de log
     */
    public MyLogger(String path, String name) {
        this.path = path;
        this.name = name;
    }
    
    /**
     *
     * @param IP IP da maquina
     */
    public MyLogger(String IP) {
        this.path = "logs/";
        this.name = IP;
        this.IpCliente = IP;
    }
    
    public void warning(String message){
        appendArquivo(IpCliente + " - " + recuperaHoraSistema() + " - WARNING - " + message);
        System.out.println(IpCliente + " - " + recuperaHoraSistema() + " - WARNING - " + message);
    }
    
    public void info(String message){
        appendArquivo(IpCliente + " - " + recuperaHoraSistema() + " - INFO - " + message);
        System.out.println(IpCliente + " - " + recuperaHoraSistema() + " - INFO - " + message);
    }
    
    public void debug(String message){
        appendArquivo(IpCliente + " - " + recuperaHoraSistema() + " - DEBUG - " + message);
        System.out.println(IpCliente + " - " + recuperaHoraSistema() + " - DEBUG - " + message);
    }
    

    public void erro(String message){
        appendArquivo(IpCliente + " - " + recuperaHoraSistema() + " - ERRO - " + message);
        System.out.println(IpCliente + " - " + recuperaHoraSistema() + " - ERRO - " + message);
    }

    private void appendArquivo(String message){
        
        FileWriter fw = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            fw = new FileWriter((path+name), true);
            bw = new BufferedWriter(fw);
            out = new PrintWriter(bw);
            out.println(message);
            out.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        } finally {
            if (out != null) {
                out.close();
            }
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
        }
        
    }
    
    private String recuperaHoraSistema(){
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        return (dtf.format(now));
        
    }
}